import os
import paho.mqtt.client as mqtt

MQTT_HOST = os.environ.get('MQTT_HOST', '0.0.0.0')

mqtt_client = mqtt.Client()
mqtt_client.connect(MQTT_HOST, 1883, 60)
