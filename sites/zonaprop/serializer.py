# -*- coding: UTF-8 -*-
import re
import logging
from sites.serializer import Serializer


class ZonaPropSerializer(Serializer):
    HARD_LIMIT_PUOFS = 10000

    def __init__(self, response):
        super(ZonaPropSerializer, self).__init__(response)
        self.populate_address()
        self.populate_price()
        self.populate_surface()
        self.populate_value_surface()
        self.obj['city'] = 'Buenos Aires'
        self.obj['source'] = 'zonaprop'

    @property
    def valid(self):
        valid = super(ZonaPropSerializer, self).valid
        if 'value_surface' in self.obj.keys():
            valid = [
                valid,
                self.obj['value_surface'] < ZonaPropSerializer.HARD_LIMIT_PUOFS
            ]
        else:
            valid = [False, valid]
        return False not in valid

    @property
    def channel(self):
        return '%s-%s' % (self.obj['operation'], self.obj['currency'])

    def populate_address(self):
        address = self.d('#go_to_map').text()
        logging.debug('Extracted address is %s' % address)
        self.obj['address'] = address

    def populate_price(self):
        # Extract price
        price = self.d('.aviso-datos ul li:eq(1) span.valor').text()
        logging.debug('Extracted price is %s' % price)
        # Extract currency
        self.obj['currency'] = 'usd' if 'U$S' in price else 'pesos'
        # Extract amount
        price = re.sub("[^0-9]", "", price)
        if price:
            self.obj['price'] = float(price)
        # Extract operation
        operation = self.d('.aviso-datos ul li:eq(1) span.nombre')
        operation = operation.text().lower()
        logging.debug('Extracted operation is %s' % operation)
        self.obj['operation'] = 'buy' if 'venta' in operation else 'rent'

    def populate_surface(self):
        # Extract cover surface
        i = 3 if self.obj['operation'] == 'buy' else 4
        cover_surface = self.d('.aviso-datos ul li:eq(%s)' % i).text().lower()
        cover_surface = re.sub("[^0-9]", "", cover_surface)
        if cover_surface:
            self.obj['cover_surface'] = float(cover_surface)
        # Extract total surface
        total_surface = self.d('.aviso-datos ul li:eq(%s)' % (i + 1))
        total_surface = total_surface.text().lower()
        total_surface = re.sub("[^0-9]", "", total_surface)
        if total_surface:
            self.obj['total_surface'] = float(total_surface)
        if 'cover_surface' in self.obj.keys():
            self.obj['surface_unit'] = 'm2'

    def populate_value_surface(self):
        validate_keys = ['cover_surface', 'price']
        validate_keys = [k in self.obj.keys() for k in validate_keys]
        if False not in validate_keys:
            if self.obj['cover_surface'] > 0 and self.obj['price'] > 0:
                self.obj['value_surface'] = self.obj['price'] / \
                    self.obj['cover_surface']
            else:
                value_surface = ZonaPropSerializer.HARD_LIMIT_PUOFS + 1
                self.obj['value_surface'] = value_surface
