import logging

from settings import mqtt_client
from sites.zonaprop.serializer import ZonaPropSerializer
from scrapy.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor


class ZonaPropSpider(CrawlSpider):
    name = 'zonaprop'
    base_uri = 'http://www.zonaprop.com.ar'
    allowed_domains = [
        'aviso.zonaprop.com.ar',
        'propiedades.zonaprop.com.ar',
        'www.zonaprop.com.ar'
    ]

    start_urls = [
        '%s/inmuebles-capital-federal.html' % base_uri,
    ]

    for i in range(1, 100):
        start_urls.append('%s/inmuebles-capital-federal-pagina-%s.html'
                          % (base_uri, i))

    rules = (
        Rule(
            LinkExtractor(
                allow='www.zonaprop.com.ar/propiedades/[0-9a-zA-Z\- ]+.html$',
            ),
            'broadcast_listing', follow=True
        ),
    )

    def broadcast_listing(self, response):
        logging.debug("Saving %s" % response.url)
        s = ZonaPropSerializer(response)
        if s.valid:
            payload = s.json
            logging.debug('Posting json paylad to mqtt')
            mqtt_client.publish(s.channel, payload=payload)
