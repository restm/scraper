from pyquery import PyQuery as pq

import simplejson as json


class Serializer(object):
    d = None
    url = None
    obj = {}
    errors = []

    def __init__(self, response):
        self.d = pq(response.body)
        self.obj = {
            'url': response.url
        }

    @property
    def channel(self):
        return 'default'

    @property
    def json(self):
        return json.dumps(self.obj)

    def __str__(self):
        return self.json

    @property
    def valid(self):
        check = ['price', 'address', 'operation', 'currency', 'city', 'source']
        check = [i in self.obj.keys() for i in check]
        return False not in check
