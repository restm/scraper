FROM registry.gitlab.com/drkloc/docker-python
MAINTAINER Teo Sibileau

ENV PYTHONUNBUFFERED 1
RUN pip install --upgrade pip

WORKDIR /code
ADD . /code/
RUN pip install -r requirements.txt
